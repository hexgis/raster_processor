#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', 'gdal>=2.0', 'homura>=0.1.5']

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', 'gdal>=2.0']

setup(
    author="Dagnaldo Silva",
    author_email='dagnaldo.silva@hexgis.com.br',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="App for processing raster images using gdal",
    entry_points={
        'console_scripts': [
            'raster_processor=raster_processor.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='raster_processor',
    name='raster_processor',
    packages=find_packages(
        include=[
            'raster_processor',
            'raster_processor.*',
            'raster_processor/tilers-tools'
        ]
    ),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/hexgis/raster_processor',
    version='0.4.11',
    zip_safe=False,
)
