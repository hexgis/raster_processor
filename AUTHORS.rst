=======
Credits
=======

Development Lead
----------------

* Dagnaldo Silva <dagnaldo.silva@hexgis.com.br>
* Victor Ferreira <victor.ferreira@hexgis.com.br>

Contributors
------------

None yet. Why not be the first?
