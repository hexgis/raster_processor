================
Raster Processor
================


.. image:: https://img.shields.io/pypi/v/raster_processor.svg
        :target: https://pypi.python.org/pypi/raster_processor

.. image:: https://img.shields.io/travis/victor.ferreira/raster_processor.svg
        :target: https://travis-ci.org/victor.ferreira/raster_processor

.. image:: https://readthedocs.org/projects/raster-processor/badge/?version=latest
        :target: https://raster-processor.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




App for processing raster images using gdal


* Free software: MIT license
* Documentation: https://raster-processor.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
