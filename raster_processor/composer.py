import os

from .utils import Utils


class Composer:
    """
    Class for processing and creating image compositions from a list of files.
    """
    @classmethod
    def set_full_output_filepath(self, filename, output_path, type_name):
        """
        Private method to set TIF output path for rgb composition

        Arguments:
            * filename (str): input file name
            * output_path (str): output file path
            * type_name (str): output band type name.
                E.g.: filename_r6g5b4

        Returns:
            * Complete filepath for rgb composition
        """
        if (filename.endswith(".TIF") or
            filename.endswith(".tif") or
            filename.endswith(".tiff") or
                filename.endswith(".TIFF")):
            file_path = os.path.join(output_path, filename)
        else:
            file_path = os.path.join(
                output_path,
                "{}_{}.TIF".format(filename, type_name)
            )

        return file_path

    @classmethod
    def get_gdal_merge_command(cls):
        return "gdal_merge.py {quiet} -separate -co PHOTOMETRIC=RGB" \
               " -o {output_path}"

    @staticmethod
    def create_composition(
        filename,
        ordered_filelist,
        out_path,
        bands,
        quiet=True
    ):
        """
        Creates a composition using gdal_merge.py with ordered filelist
        check https://gdal.org/programs/gdal_merge.html for more information

        Arguments:
            * filename (str): output filename. E.g.: my_file, my_file.tif
            * ordered_filelist (str): list of images used on merge,
                must be ordered for correct output
            * out_path (str): expected output path image
            * bands (str): list bands number for composition.
                E.g.: 6,5,4 -> r6g5b4

        Raises:
            * error (ValueError): dataset is not valid
            * error (ValueError): bands length different of images length

        Returns:
            * filepath (str): merged image
        """
        type_name = "r{0}g{1}b{2}".format(*bands)

        filepath = Composer.set_full_output_filepath(
            filename=filename,
            output_path=out_path,
            type_name=type_name
        )

        if not quiet:
            print("-- Creating file composition to {}".format(filepath))
            quiet = ""
        else:
            quiet = "-q"

        command = Composer.get_gdal_merge_command()
        command = command.format(quiet=quiet, output_path=filepath)

        for file in ordered_filelist:
            command += " " + file

        if not Utils._subprocess(command):
            log = "log while executting command {}".format(command)
            raise ValueError(log)

        is_valid = Utils.validate_image_bands(filepath, ordered_filelist)

        if is_valid:
            return {
                "name": filepath.split("/")[-1],
                "path": filepath,
                "type": type_name
            }
        else:
            return None
