#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utils used on raster_processor module
"""
import os
import subprocess
import shutil
import tempfile
from datetime import datetime

try:
    from osgeo import gdal
except ImportError as error:
    print("Error importing GDAL {}".format(error))
    subprocess.call("python --version", shell=True)
    import gdal


class Utils:
    """ Misc utility methods """

    @staticmethod
    def _print(msg, quiet):
        """
        Used to print if quiet is False

        Arguments:
            * msg: shell command message
            * quiet: if quiet is True hides the shell message
        """
        if not quiet:
            print(msg)

    @staticmethod
    def _subprocess(command):
        """
        Function to check_call subprocess

        Arguments:
            * command: command for subprocess

        Returns:
            * if command shell=True return ok=True
        """
        ok = True

        try:
            subprocess.check_call(command, shell=True)
        except subprocess.CalledProcessError as exc:
            print(exc)
            ok = False
        except OSError as exc:
            print(exc)
            ok = False

        return ok

    @staticmethod
    def move_path_files(src, dest, src_remove=True):
        """
        Void method to Move files from source directory to
        destination directory using shutil to move only files from path

        Arguments:
            * src (str): source path
            * dest (str): destination path
            * src_remove (bool): remove files after copy? Default: True
        """
        if os.path.isdir(src):
            dir_path = os.path.basename(src)
            if dir_path in os.listdir(dest):
                shutil.rmtree(os.path.join(dest, dir_path))
            shutil.move(src, dest)
        if os.path.isfile(src):
            shutil.copy(src, dest)

        if src_remove:
            if os.path.isdir(src):
                shutil.rmtree(src)
            else:
                Utils.remove_file(src)

    @staticmethod
    def create_tempdir():
        """
        Creates a tempdir
        Always return folder_path.
        """
        date = datetime.strftime(datetime.now(), "%Y_%m_%d")
        return tempfile.mkdtemp(suffix='_{}_tiler'.format(date))

    @staticmethod
    def check_creation_folder(folder):
        """
        Check whether a folder exists, if not the folder is created.

        Arguments:
            * folder: path of folder

        Returns:
            * Always return folder_path.
        """
        if not os.path.exists(folder):
            os.makedirs(folder)

        return folder

    @staticmethod
    def validate_image_bands(image, data):
        """
        Check if the image exists and then check its bands

        If is a valid datasource with same bands num of data (nodata)

        Arguments:
            * image: image to be checked
            * data: data for comparison

        Returns:
            * if image bands equals len(data) return true, if not, return false
        """

        if not os.path.isfile(image):
            return False

        try:
            ds = gdal.Open(image)
        except Exception as exc:
            print(exc)
            return False

        return ds.RasterCount == len(data)

    @staticmethod
    def remove_file(input_file):
        """
        Static method for remove files

        Arguments:
            * input_file: file to be checked

        Returns:
            * if path to the file exists, remove the file
        """
        if os.path.exists(input_file):
            return os.remove(input_file)
